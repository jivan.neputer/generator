@extends('admin.layouts.layout')

@push('style')

@endpush

@section('title', 'my-category')

@section('content')

@include('admin.common.breadcrumbs', [
'title' => 'Show',
'panel' => 'my-category',
'id'    => $data['my-category']->id,
])

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">

                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th width="150px">Label</th>
                            <th>Information</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
    <td style="width:35%;">Category_name</td>
    <td style="width:65%">{{ $data['my-category']->category_name }}</td>
</tr>
<tr>
    <td style="width:35%;">Address</td>
    <td style="width:65%">{{ $data['my-category']->address }}</td>
</tr>
<tr>
    <td style="width:35%;">Created_by</td>
    <td style="width:65%">{{ $data['my-category']->created_by }}</td>
</tr>
<tr>
    <td style="width:35%;">Updated_by</td>
    <td style="width:65%">{{ $data['my-category']->updated_by }}</td>
</tr>
<tr>
    <td style="width:35%;">Slug</td>
    <td style="width:65%">{{ $data['my-category']->slug }}</td>
</tr>
<tr>
    <td style="width:35%;">Description</td>
    <td style="width:65%">{{ $data['my-category']->description }}</td>
</tr>
<tr>
    <td style="width:35%;">Status</td>
    <td style="width:65%">{{ $data['my-category']->status }}</td>
</tr>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('js')

@endpush
