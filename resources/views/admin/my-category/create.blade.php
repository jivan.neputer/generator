@extends('admin.layouts.layout')

@push('style')
@endpush

@section('title', 'Create a my-category')

@section('content')
    @include('admin.common.breadcrumbs', [
        'title'=> 'Create',
        'panel'=> 'my-category',
    ])

    <div class="wrapper wrapper-content">
        {!! Form::open(['route' => 'admin.my-category.store', 'enctype' => 'multipart/form-data', 'method' => 'post', 'id' => 'my-categoryForm']) !!}
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>General Info</h5>
                    </div>
                    <div class="ibox-content">
                        @include('admin.my-category.partials.form')
                    </div>
                </div>
            </div>
        </div>
        @includeIf('admin.common.action')
        {!! Form::close() !!}
    </div>
@endsection

@push('js')
    <script src="{{ asset('assets/admin/plugins/validate/jquery.validate.min.js') }}"></script>
    @include('admin.my-category.partials.script')
@endpush
