<div class="hr-line-dashed"></div>
<div class="form-group  row">
    <label class="col-sm-2 col-form-label is_required">category_name</label>
    <div class="col-sm-6 {{ $errors->has('category_name') ? 'has-error' : '' }}">
        {!! Form::text('category_name', null, [ 'class' => 'form-control' ]) !!}
        @if($errors->has('category_name'))
            <label class="has-error" for="category_name">{{ $errors->first('category_name') }}</label>
        @endif
    </div>
</div>

<div class="hr-line-dashed"></div>
<div class="form-group  row">
    <label class="col-sm-2 col-form-label is_required">address</label>
    <div class="col-sm-6 {{ $errors->has('address') ? 'has-error' : '' }}">
        {!! Form::text('address', null, [ 'class' => 'form-control' ]) !!}
        @if($errors->has('address'))
            <label class="has-error" for="address">{{ $errors->first('address') }}</label>
        @endif
    </div>
</div>

<div class="hr-line-dashed"></div>
<div class="form-group  row">
    <label class="col-sm-2 col-form-label is_required">description</label>
    <div class="col-sm-6 {{ $errors->has('description') ? 'has-error' : '' }}">
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        @if($errors->has('description'))
            <label class="has-error" for="description">{{ $errors->first('description') }}</label>
        @endif
    </div>
</div>

<div class="hr-line-dashed"></div>
<div class="form-group row">
    <label class="col-sm-1 col-form-label is_required">status</label>
    <div class="col-sm-8">
        <div class="i-checks mt-2">
            <label for="active"> {!! Form::radio('status', 1, true, ['id' => 'active']) !!} <i></i> Active </label>
            <label for="inactive"> {!! Form::radio('status', 0, false, ['id' => 'inactive']) !!} <i></i> Inactive </label>
        </div>
    </div>
</div>

