@extends('admin.layouts.layout')

@push('style')

@endpush

@section('title', 'Edit a my-category')

@section('content')

    @include('admin.common.breadcrumbs', [
        'title'=> 'Edit',
        'panel'=> 'my-category',
        'id'=> $data['my-category']->id,
    ])

    <div class="wrapper wrapper-content">
        {!! Form::model($data['my-category'],['route' => ['admin.my-category.update',$data['my-category']->id], 'enctype' => 'multipart/form-data', 'method' => 'put', 'id' => 'my-categoryForm']) !!}
        {!! Form::hidden('id', $data['my-category']->id) !!}
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>General Info</h5>
                    </div>
                    <div class="ibox-content">
                        @includeIf('admin.my-category.partials.form')
                    </div>
                </div>
            </div>

        </div>
        @includeIf('admin.common.action')
        {!! Form::close() !!}
    </div>

@endsection

@push('js')
    <script src="{{ asset('assets/admin/plugins/validate/jquery.validate.min.js') }}"></script>
    @include('admin.my-category.partials.script')
@endpush
