@extends('admin.layouts.layout')

@push('style')

@endpush

@section('title', 'product')

@section('content')

@include('admin.common.breadcrumbs', [
'title' => 'Show',
'panel' => 'product',
'id'    => $data['product']->id,
])

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">

                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th width="150px">Label</th>
                            <th>Information</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
    <td style="width:35%;">Category Name</td>
    <td style="width:65%">{{ $data['product']->category_name }}</td>
</tr>
<tr>
    <td style="width:35%;">Slug</td>
    <td style="width:65%">{{ $data['product']->slug }}</td>
</tr>
<tr>
    <td style="width:35%;">Address</td>
    <td style="width:65%">{{ $data['product']->address }}</td>
</tr>
<tr>
    <td style="width:35%;">Email</td>
    <td style="width:65%">{{ $data['product']->email }}</td>
</tr>
<tr>
    <td style="width:35%;">Price</td>
    <td style="width:65%">{{ $data['product']->price }}</td>
</tr>
<tr>
    <td style="width:35%;">Description</td>
    <td style="width:65%">{{ $data['product']->description }}</td>
</tr>
<tr>
    <td style="width:35%;">Status</td>
    <td style="width:65%">{{ $data['product']->status }}</td>
</tr>
<tr>
    <td style="width:35%;">Created By</td>
    <td style="width:65%">{{ $data['product']->created_by }}</td>
</tr>
<tr>
    <td style="width:35%;">Updated By</td>
    <td style="width:65%">{{ $data['product']->updated_by }}</td>
</tr>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('js')

@endpush
