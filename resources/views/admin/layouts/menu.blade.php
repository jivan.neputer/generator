<li class="{{ (request()->is('admin/car*')) ? 'active' : '' }}">
    <a href="#" aria-expanded="false">
        <i class="fa fa-bar-chart-o"></i> <span class="nav-label">Car </span><span class="fa arrow"></span>
    </a>
    <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 0px;">
        <li class="{{ (request()->is('admin/car')) ? 'active' : '' }}"><a href="{{ route('admin.car.index') }}">List</a></li>
        <li class="{{ (request()->is('admin/car/create')) ? 'active' : '' }}"><a href="{{ route('admin.car.create') }}">Add</a></li>
    </ul>
</li>


<li class="{{ (request()->is('admin/my-category*')) ? 'active' : '' }}">
    <a href="#" aria-expanded="false">
        <i class="fa fa-bar-chart-o"></i> <span class="nav-label">My Category </span><span class="fa arrow"></span>
    </a>
    <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 0px;">
        <li class="{{ (request()->is('admin/my-category')) ? 'active' : '' }}"><a href="{{ route('admin.my-category.index') }}">List</a></li>
        <li class="{{ (request()->is('admin/my-category/create')) ? 'active' : '' }}"><a href="{{ route('admin.my-category.create') }}">Add</a></li>
    </ul>
</li>

<li class="{{ (request()->is('admin/product*')) ? 'active' : '' }}">
    <a href="#" aria-expanded="false">
        <i class="fa fa-bar-chart-o"></i> <span class="nav-label">Product </span><span class="fa arrow"></span>
    </a>
    <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 0px;">
        <li class="{{ (request()->is('admin/product')) ? 'active' : '' }}"><a href="{{ route('admin.product.index') }}">List</a></li>
        <li class="{{ (request()->is('admin/product/create')) ? 'active' : '' }}"><a href="{{ route('admin.product.create') }}">Add</a></li>
    </ul>
</li>

