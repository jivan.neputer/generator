<div class="hr-line-dashed"></div>
<div class="form-group  row">
    <label class="col-sm-2 col-form-label is_required">Category</label>
    <div class="col-sm-6 {{ $errors->has('category_name') ? 'has-error' : '' }}">
        {!! Form::text('category_name', null, [ 'class' => 'form-control', 'placeholder'=>'plz inter your address' ]) !!}
        @if($errors->has('category_name'))
            <label class="has-error" for="category_name">{{ $errors->first('category_name') }}</label>
        @endif
    </div>
</div>

<div class="hr-line-dashed"></div>
<div class="form-group  row">
    <label class="col-sm-2 col-form-label is_required">Address</label>
    <div class="col-sm-6 {{ $errors->has('address') ? 'has-error' : '' }}">
        {!! Form::text('address', null, [ 'class' => 'form-control', 'placeholder'=>'plz inter your address' ]) !!}
        @if($errors->has('address'))
            <label class="has-error" for="address">{{ $errors->first('address') }}</label>
        @endif
    </div>
</div>

<div class="hr-line-dashed"></div>
<div class="form-group  row">
    <label class="col-sm-2 col-form-label is_required">Email</label>
    <div class="col-sm-6 {{ $errors->has('email') ? 'has-error' : '' }}">
        {!! Form::email('email', null, [ 'class' => 'form-control', 'placeholder'=>'please enter email' ]) !!}
        @if($errors->has('email'))
            <label class="has-error" for="email">{{ $errors->first('email') }}</label>
        @endif
    </div>
</div>

<div class="hr-line-dashed"></div>
<div class="form-group  row">
    <label class="col-sm-2 col-form-label is_required">Password</label>
    <div class="col-sm-6 {{ $errors->has('user_password') ? 'has-error' : '' }}">
        {!! Form::password('user_password', [ 'class' => 'form-control' ]) !!}
        @if($errors->has('user_password'))
            <label class="has-error" for="user_password">{{ $errors->first('user_password') }}</label>
        @endif
    </div>
</div>

<div class="hr-line-dashed"></div>
<div class="form-group  row">
    <label class="col-sm-2 col-form-label is_required">Price</label>
    <div class="col-sm-6 {{ $errors->has('price') ? 'has-error' : '' }}">
        {!! Form::number('price', null,['min'=>0,'class' => 'form-control', 'placeholder'=>'please enter price']) !!}
        @if($errors->has('price'))
            <label class="has-error" for="price">{{ $errors->first('price') }}</label>
        @endif
    </div>
</div>

<div class="hr-line-dashed"></div>
<div class="form-group  row">
    <label class="col-sm-2 col-form-label is_required">Image</label>
    <div class="col-sm-6 {{ $errors->has('image') ? 'has-error' : '' }}">
        {!! Form::file('image', [ 'class' => 'form-control' ]) !!}
        @if($errors->has('image'))
            <label class="has-error" for="image">{{ $errors->first('image') }}</label>
        @endif
    </div>
</div>

<div class="hr-line-dashed"></div>
<div class="form-group  row">
    <label class="col-sm-2 col-form-label is_required">Description</label>
    <div class="col-sm-6 {{ $errors->has('description') ? 'has-error' : '' }}">
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        @if($errors->has('description'))
            <label class="has-error" for="description">{{ $errors->first('description') }}</label>
        @endif
    </div>
</div>

<div class="hr-line-dashed"></div>
<div class="form-group  row">
    <label class="col-sm-2 col-form-label is_required">Date</label>
    <div class="col-sm-6 {{ $errors->has('date') ? 'has-error' : '' }}">
        {!! Form::date('date',$data['date'] ?? null, ['class' => 'form-control']) !!}
        {{--        {!! Form::date('basic[deadline]', $data['job']->deadline ?? null, ['class' => 'form-control']) !!}--}}
        {{--        @error('basic.deadline')--}}
    @if($errors->has('date'))
            <label class="has-error" for="date">{{ $errors->first('date') }}</label>
        @endif
    </div>
</div>

<div class="hr-line-dashed"></div>
<div class="form-group row">
    <label class="col-sm-1 col-form-label is_required">Form Status</label>
    <div class="col-sm-8">
        <div class="i-checks mt-2">
            <Active for="active"> {!! Form::radio('status', 0, false, ['id' => 'active']) !!} <i></i> Active </Active>
<In-active for="in-active"> {!! Form::radio('status', 1, true, ['id' => 'in-active']) !!} <i></i> In-active </In-active>

        </div>
    </div>
</div>

<div class="hr-line-dashed"></div>
<div class="form-group row">
    <label class="col-sm-1 col-form-label is_required">Position</label>
    <div class="col-sm-8">
        <div class="i-checks mt-2">
            <Admin for="admin"> {!! Form::checkbox('position', 'a', true, ['id' => 'admin']) !!} <i></i> Admin </Admin>
<User for="user"> {!! Form::checkbox('position', 'u', false, ['id' => 'user']) !!} <i></i> User </User>

        </div>
    </div>
</div>

