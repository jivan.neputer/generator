<script>
    $(function () {
        $("#carForm").validate({
            rules: {
                name: "required",
            },
            messages: {
                name: "Please enter car name",
            }
        });
    })
</script>
