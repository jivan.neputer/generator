@extends('admin.layouts.layout')

@push('style')
@endpush

@section('title', 'Edit a car')

@section('content')

    @include('admin.common.breadcrumbs', [
        'title'=> 'Edit',
        'panel'=> 'car',
        'id'=> $data['car']->id,
    ])

    <div class="wrapper wrapper-content">
        {!! Form::model($data['car'],['route' => ['admin.car.update',$data['car']->id], 'enctype' => 'multipart/form-data', 'method' => 'put', 'id' => 'carForm']) !!}
        {!! Form::hidden('id', $data['car']->id) !!}
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>General Info</h5>
                    </div>
                    <div class="ibox-content">
                        @includeIf('admin.car.partials.form')
                    </div>
                </div>
            </div>

        </div>
        @includeIf('admin.common.action')
        {!! Form::close() !!}
    </div>

@endsection

@push('js')
    <script src="{{ asset('assets/admin/plugins/validate/jquery.validate.min.js') }}"></script>
    @include('admin.car.partials.script')
@endpush
