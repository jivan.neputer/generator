@extends('admin.layouts.layout')

@push('style')
@endpush

@section('title', 'Create a car')

@section('content')
    @include('admin.common.breadcrumbs', [
        'title'=> 'Create',
        'panel'=> 'car',
    ])

    <div class="wrapper wrapper-content">
        {!! Form::open(['route' => 'admin.car.store', 'enctype' => 'multipart/form-data', 'method' => 'post', 'id' => 'carForm']) !!}
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>General Info</h5>
                    </div>
                    <div class="ibox-content">
                        @include('admin.car.partials.form')
                    </div>
                </div>
            </div>
        </div>
        @includeIf('admin.common.action')
        {!! Form::close() !!}
    </div>
@endsection

@push('js')
    <script src="{{ asset('assets/admin/plugins/validate/jquery.validate.min.js') }}"></script>
    @include('admin.car.partials.script')
@endpush
