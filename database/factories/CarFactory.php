<?php

/** @var Factory $factory */

use App\Models\Car;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Car::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->sentence,
        'status' => mt_rand(0,1)
    ];
});
