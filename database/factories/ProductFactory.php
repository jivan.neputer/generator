<?php

/** @var Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Product::class, function (Faker $faker) {
    return [
    'category_name' => $faker->firstName,
    'slug' => $faker->slug,
    'address' => $faker->address,
    'description' => $faker->sentence,
    'email' => $faker->email,
    'user_password' => $faker->password,
    'price' => rand(99,999),
    'date' => $faker->date,
    'status' => rand(0,1),
    'created_by' => rand(0,1)
    ];
});
