<?php

/** @var Factory $factory */

use App\Models\MyCategory;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(MyCategory::class, function (Faker $faker) {
    return [
        'category_name' => $faker->firstName,
        'slug' => $faker->slug,
        'address' => $faker->address,
        'description' => $faker->sentence(),
        'status' => rand(0, 1),
        'created_by'    => rand(1,10),
    ];
});
