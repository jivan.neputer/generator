<?php

use App\Models\MyCategory;
use Illuminate\Database\Seeder;

class MyCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(MyCategory::class, 5)->create();
    }
}
