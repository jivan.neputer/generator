# Instructions
    Before using command : php artisan genji:generator modelName
    Ex:- php artisan genji:generator product
    
    Make Sure :
    - Composer install
    - And give desire input name that you want that model to form.yml
    - Input columns in form.yml
    
### Form Yml :
    - There is main 3 section
    - 1 section is about the permission, if we set true then generator
      will run that model belongs code, if we set false then, that belonged
      code will not run.
    - 2 section is about view. where we can define field for inde,create and view
      (Here Pipe '|' in form section is if we have to create multiple colums)
      (Here Pipe '|' in showTable is help to define which colums should be define in show or table or in both)
    - 3 sectionn is about Column for model , migration and factory
      ( Pipe (|) can be used if we have multiple columns )
    NOTE 
        #generate
            # Here generate is a area where we can define which section should execute or not
            # If we give a value true then that section will execute and if we give value false then section remain salient
            # that means that section will not execute in generator.
        
        #select: Yes
        
        # working migration type
          #category_name: "category_name|unsignedInteger|default|0"
          #type: "type|tinyInteger|default|1|comment|'comment text'"
        
        # Factory
          # just put the value as you give in factory But inside double quote.
          #category_name: "$faker->firstName"   slug: "$faker->slug"
          #address: "$faker->address"   description: "$faker->sentence"   status: "rand(0,1)"



### Instructions to be followed after generating :
    - Add validations in client side and backend both [ Store/UpdateRequest & scripts.blade.php ]

### How to Access :
    - Your URL is:  hostname/admin/car
    - Ex: http://127.0.0.1:8000/admin/car 
     (here car and product are already created)
   

### Bug &  Fixes :
    1. Suggest me....


#### Enjoy it, Happy Codding...
