<?php

namespace App\Services;

use App\Models\Product;
use App\Generator\Base\BaseService;

/**
 * Class ProductService
 * @package Foundation\Services
 */
class ProductService extends BaseService
{

    /**
     * The Product instance
     *
     * @var $model
     */
    protected $model;

    /**
     * ProductService constructor.
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->model = $product;
    }

    /**
     * Filter
     *
     * @param string|null $name
     * @return mixed
     */
    public function filter(string $name = null)
    {
        return $this->model
            ->where(function ($query) use ($name){
                if($name){
                    $query->where('name','like', '%'. $name .'%');
                }
            })
            ->get();
    }

}
