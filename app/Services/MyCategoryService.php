<?php

namespace App\Services;

use App\Models\Car;
use App\Models\MyCategory;
use App\Generator\Base\BaseService;

/**
 * Class MyCategoryService
 * @package Foundation\Services
 */
class MyCategoryService extends BaseService
{

    /**
     * The MyCategory instance
     *
     * @var $model
     */
    protected $model;

    /**
     * MyCategoryService constructor.
     * @param MyCategory $myCategory
     */
    public function __construct(MyCategory $myCategory)
    {
        $this->model = $myCategory;
    }

    /**
     * Filter
     *
     * @param string|null $name
     * @return mixed
     */
    public function filter(string $name = null)
    {
        return $this->model
            ->where(function ($query) use ($name){
                if($name){
                    $query->where('name','like', '%'. $name .'%');
                }
            })
            ->get();
    }





}
