<?php

namespace App\Services;

use App\Models\Car;
use App\Generator\Base\BaseService;

/**
 * Class CarService
 * @package Foundation\Services
 */
class CarService extends BaseService
{

    /**
     * The Car instance
     *
     * @var $model
     */
    protected $model;

    /**
     * CarService constructor.
     * @param Car $car
     */
    public function __construct(Car $car)
    {
        $this->model = $car;
    }

    /**
     * Filter
     *
     * @param string|null $name
     * @return mixed
     */
    public function filter(string $name = null)
    {
        return $this->model
            ->where(function ($query) use ($name){
                if($name){
                    $query->where('name','like', '%'. $name .'%');
                }
            })
            ->get();
    }

}
