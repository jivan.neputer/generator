<?php

namespace App\Models;

use App\Generator\Base\BaseModel;

/**
 * Class Car
 * @package Foundation\Models
 */
class Car extends BaseModel
{
    protected $fillable = [
            'name', 'description', 'status'
    ];
}
