<?php

namespace App\Models;

use App\Generator\Base\BaseModel;

/**
 * Class MyCategory
 * @package Foundation\Models
 */
class MyCategory extends BaseModel
{

    protected $table = 'my_categories';
    protected $fillable = [
            'category_name', 'address', 'created_by', 'updated_by', 'slug', 'description', 'status'
    ];
}
