<?php

namespace App\Models;

use App\Generator\Base\BaseModel;

/**
 * Class Product
 * @package Foundation\Models
 */
class Product extends BaseModel
{
    protected $fillable = [
            'category_name', 'address', 'created_by', 'updated_by', 'slug', 'description', 'email', 'user_password', 'price', 'image', 'date', 'status'
    ];
}
