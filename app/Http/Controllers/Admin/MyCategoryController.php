<?php
namespace App\Http\Controllers\Admin;

use App\Models\MyCategory;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use App\Generator\Base\BaseController;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Illuminate\Contracts\View\Factory;
use App\Services\MyCategoryService;
use App\Http\Requests\MyCategory\{
    StoreRequest,
    UpdateRequest
};
use Illuminate\Http\RedirectResponse;
use Session;

/**
 * Class MyCategoryController
 * @package Foundation\Controllers
 */
class MyCategoryController extends BaseController
{

    /**
     * The MyCategoryService instance
     *
     * @var $myCategoryService
     */
    private $myCategoryService;

    public function __construct(MyCategoryService $myCategoryService)
    {
        $this->myCategoryService = $myCategoryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Application|Factory|View
     * @throws Exception
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return datatables()
                ->of($this->myCategoryService->filter($request->input('search.value')))
                ->addColumn('created_at', function ($data) {
                    return $data->created_at . " <code>{$data->created_at->diffForHumans()}</code>";
                })
                ->addColumn('action', function ($data) {
                    $model = 'my-category';
                    return view('admin.common.datatable.action', compact('data', 'model'))->render();
                })
                ->rawColumns([ 'action', 'created_at', ])
                ->make(true);
        }

        return view('admin.my-category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.my-category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest $request
     * @return RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $this->myCategoryService->new($request->merge([
            'slug' => Str::slug($request->get('category_name')),
        ])->all());
        Session::flash('success','Record successfully created.');
        return $this->redirect($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  MyCategory $myCategory
     * @return Factory
     */
    public function show(MyCategory $myCategory)
    {
        $data = [];
        $data['my-category'] = $myCategory;
        return view('admin.my-category.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  MyCategory $myCategory
     * @return Factory
     */
    public function edit(MyCategory $myCategory)
    {
        $data = [];
        $data['my-category']  = $myCategory;
        return view('admin.my-category.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  MyCategory $myCategory
     * @return RedirectResponse
     */
    public function update(UpdateRequest $request, MyCategory $myCategory)
    {
        $this->myCategoryService->update($request->all(), $myCategory);
        Session::flash('success','Record successfully updated.');
        return $this->redirect($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  MyCategory $myCategory
     * @return RedirectResponse
     */
    public function destroy(MyCategory $myCategory)
    {
        $this->myCategoryService->delete($myCategory);
        Session::flash('success','Record deleted successfully !');
        return redirect()->back();
    }
}
