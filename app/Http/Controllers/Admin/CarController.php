<?php
namespace App\Http\Controllers\Admin;

use App\Models\Car;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use App\Generator\Base\BaseController;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Illuminate\Contracts\View\Factory;
use App\Services\CarService;
use App\Http\Requests\Car\{
    StoreRequest,
    UpdateRequest
};
use Illuminate\Http\RedirectResponse;
use Session;

/**
 * Class CarController
 * @package Foundation\Controllers
 */
class CarController extends BaseController
{

    /**
     * The CarService instance
     *
     * @var $carService
     */
    private $carService;

    public function __construct(CarService $carService)
    {
        $this->carService = $carService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Application|Factory|View
     * @throws Exception
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return datatables()
                ->of($this->carService->filter($request->input('search.value')))
                ->addColumn('created_at', function ($data) {
                    return $data->created_at . " <code>{$data->created_at->diffForHumans()}</code>";
                })
                ->addColumn('action', function ($data) {
                    $model = 'car';
                    return view('admin.common.datatable.action', compact('data', 'model'))->render();
                })
                ->rawColumns([ 'action', 'created_at', ])
                ->make(true);
        }

        return view('admin.car.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $data['tag'] = [];
        return view('admin.car.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest $request
     * @return RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $this->carService->new($request->all());
        Session::flash('success','Record successfully created.');
        return $this->redirect($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  Car $car
     * @return Factory
     */
    public function show(Car $car)
    {
        $data = [];
        $data['car'] = $car;
        return view('admin.car.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Car $car
     * @return Factory
     */
    public function edit(Car $car)
    {
        $data = [];
        $data['car']  = $car;
        return view('admin.car.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  Car $car
     * @return RedirectResponse
     */
    public function update(UpdateRequest $request, Car $car)
    {
        $this->carService->update($request->all(), $car);
        Session::flash('success','Record successfully updated.');
        return $this->redirect($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Car $car
     * @return RedirectResponse
     */
    public function destroy(Car $car)
    {
        $this->carService->delete($car);
        Session::flash('success','Record deleted successfully !');
        return redirect()->back();
    }
}
