<?php
namespace App\Http\Controllers\Admin;

use App\Models\Product;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use App\Generator\Base\BaseController;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Illuminate\Contracts\View\Factory;
use App\Services\ProductService;
use App\Http\Requests\Product\{
    StoreRequest,
    UpdateRequest
};
use Illuminate\Http\RedirectResponse;
use Session;

/**
 * Class ProductController
 * @package Foundation\Controllers
 */
class ProductController extends BaseController
{

    /**
     * The ProductService instance
     *
     * @var $productService
     */
    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Application|Factory|View
     * @throws Exception
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return datatables()
                ->of($this->productService->filter($request->input('search.value')))
                ->addColumn('created_at', function ($data) {
                    return $data->created_at . " <code>{$data->created_at->diffForHumans()}</code>";
                })
                ->addColumn('action', function ($data) {
                    $model = 'product';
                    return view('admin.common.datatable.action', compact('data', 'model'))->render();
                })
                ->rawColumns([ 'action', 'created_at', ])
                ->make(true);
        }

        return view('admin.product.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest $request
     * @return RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $this->productService->new($request->all());
        Session::flash('success','Record successfully created.');
        return $this->redirect($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  Product $product
     * @return Factory
     */
    public function show(Product $product)
    {
        $data = [];
        $data['product'] = $product;
        return view('admin.product.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Product $product
     * @return Factory
     */
    public function edit(Product $product)
    {
        $data = [];
        $data['product']  = $product;
        return view('admin.product.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  Product $product
     * @return RedirectResponse
     */
    public function update(UpdateRequest $request, Product $product)
    {
        $this->productService->update($request->all(), $product);
        Session::flash('success','Record successfully updated.');
        return $this->redirect($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Product $product
     * @return RedirectResponse
     */
    public function destroy(Product $product)
    {
        $this->productService->delete($product);
        Session::flash('success','Record deleted successfully !');
        return redirect()->back();
    }
}
