<li class="{{ (request()->is('admin/{menu}*')) ? 'active' : '' }}">
    <a href="#" aria-expanded="false">
        <i class="fa fa-bar-chart-o"></i> <span class="nav-label">{MENU} </span><span class="fa arrow"></span>
    </a>
    <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 0px;">
        <li class="{{ (request()->is('admin/{menu}')) ? 'active' : '' }}"><a href="{{ route('admin.{menu}.index') }}">List</a></li>
        <li class="{{ (request()->is('admin/{menu}/create')) ? 'active' : '' }}"><a href="{{ route('admin.{menu}.create') }}">Add</a></li>
    </ul>
</li>

