<div class="hr-line-dashed"></div>
<div class="form-group  row">
    <label class="col-sm-2 col-form-label is_required">labelName</label>
    <div class="col-sm-6 {{ $errors->has('columnName') ? 'has-error' : '' }}">
        {!! Form::number('columnName', null,['min'=>0,'class' => 'form-control', placeholder]) !!}
        @if($errors->has('columnName'))
            <label class="has-error" for="columnName">{{ $errors->first('columnName') }}</label>
        @endif
    </div>
</div>
