<?php


namespace App\Foundation\Supports\Generator\Handlers;


final class PathHandler
{

    public static function getDirectory($directory){
        if($directory == 'database'){
            return  'database';
        }
        if($directory == 'migration'){
            return  'migrations';
        }
        if($directory == 'factory'){
            return  'factories';
        }
        if($directory == 'seeder'){
            return  'seeds';
        }
        if($directory == 'http'){
            return  'Http';
        }
        if($directory == 'model'){
            return  'Models';
        }
        if($directory == 'service'){
            return  'Services';
        }
        if($directory == 'request'){
            return  'Requests';
        }
        if($directory == 'controller'){
            return  'Controllers';
        }
        if($directory == 'resource'){
            return  'resources';
        }
        if($directory == 'view'){
            return  'views';
        }
        if($directory == 'view-admin'){
            return  'admin';
        }
        if($directory == 'route'){
            return  'routes';
        }
        if($directory == 'foundation'){
            return  'Foundation';
        }
        if($directory == 'views'){
            return  'Views';
        }
        if($directory == 'form'){
            return  'Form';
        }
        if($directory == 'radio'){
            return  'Radio';
        }
        if($directory == 'checkbox'){
            return  'Checkbox';
        }

    }

    public  static  function  getPath($fileName){
        $database = self::getDirectory('database');
        $migrationName = self::getDirectory('migration');
        $factoryName = self::getDirectory('factory');
        $seederName = self::getDirectory('seeder');
        $http = self::getDirectory('http');
        $model = self::getDirectory('model');
        $service = self::getDirectory('service');
        $request = self::getDirectory('request');
        $controller = self::getDirectory('controller');
        $resource = self::getDirectory('resource');
        $view = self::getDirectory('view');
        $admin = self::getDirectory('view-admin');
        $route = self::getDirectory('route');

        /* Inside Foundation */
        $foundation = self::getDirectory('foundation');
        $views = self::getDirectory('views');
        $form = self::getDirectory('form');
        $radio = self::getDirectory('radio');
        $checkbox = self::getDirectory('checkbox');



        if($fileName == 'migration'){
            return base_path() . DIRECTORY_SEPARATOR . $database . DIRECTORY_SEPARATOR . $migrationName;
        }
        if($fileName == 'factory'){
            return base_path() . DIRECTORY_SEPARATOR . $database . DIRECTORY_SEPARATOR . $factoryName;
        }
        if($fileName == 'seeder') {
            return base_path() . DIRECTORY_SEPARATOR . $database . DIRECTORY_SEPARATOR . $seederName;
        }
        if($fileName == 'model') {
            return app_path() . DIRECTORY_SEPARATOR . $model ;
        }
        if($fileName == 'service') {
            return app_path() . DIRECTORY_SEPARATOR . $service ;
        }
        if($fileName == 'request') {
            return app_path() . DIRECTORY_SEPARATOR . $http . DIRECTORY_SEPARATOR . $request ;
        }
        if($fileName == 'controller') {
            return app_path() . DIRECTORY_SEPARATOR . $http . DIRECTORY_SEPARATOR . $controller .DIRECTORY_SEPARATOR .'Admin';
        }
        if($fileName == 'view') {
            return resource_path() . DIRECTORY_SEPARATOR . $view . DIRECTORY_SEPARATOR . $admin ;
        }
        if($fileName == 'appendMenu'){
            return resource_path() . DIRECTORY_SEPARATOR . $view . DIRECTORY_SEPARATOR . $admin . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'menu.blade.php' ;
        }
        if($fileName == 'appendSeeder'){
            return base_path() . DIRECTORY_SEPARATOR . $database . DIRECTORY_SEPARATOR . $seederName . DIRECTORY_SEPARATOR . 'DatabaseSeeder.php';
        }
        if($fileName == 'route') {
            return base_path() . DIRECTORY_SEPARATOR . $route . DIRECTORY_SEPARATOR . 'admin.php' ;
        }
        if($fileName == 'radio') {
            return app_path() . DIRECTORY_SEPARATOR . $foundation . DIRECTORY_SEPARATOR . $views . DIRECTORY_SEPARATOR . $form . DIRECTORY_SEPARATOR . $radio ;
        }
        if($fileName == 'checkbox') {
            return app_path() . DIRECTORY_SEPARATOR . $foundation . DIRECTORY_SEPARATOR . $views . DIRECTORY_SEPARATOR . $form . DIRECTORY_SEPARATOR . $checkbox ;
        }

    }
}
