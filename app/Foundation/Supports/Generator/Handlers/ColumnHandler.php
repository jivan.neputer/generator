<?php


namespace App\Foundation\Supports\Generator\Handlers;

use App\Foundation\Supports\Generator\Handlers\View\Builder;

/**
 * Class ColumnHandler
 * @package App\Foundation\Supports\Generator\Handlers
 */
final class ColumnHandler
{
    /**
     * @return string
     */
    public static function handle(){
        return "'" . implode ( "', '", Builder::getColumns() ) . "'";
    }

}
