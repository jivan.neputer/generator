<?php


namespace App\Foundation\Supports\Generator\Handlers;


use App\Foundation\Supports\Generator\Handlers\View\Builder;
use Illuminate\Support\Str;

final class MigrationHandler
{
    public static function getMigration($argumentName){
        $className = BaseHandler::checkClassName($argumentName);
        BaseHandler::checkDuplicateEntry($modelName = $className, $fileName = 'migration');
        $tableName = self::resolveMigrationClassName($className);
        $fileName = self::resolveFileName($tableName);
        $migrationContain = self::getMigrationContain();
        $migrationTemplate = self::solveStub($tableName, $migrationContain, $fileType = 'Migration');

        $folderPath = base_path().DIRECTORY_SEPARATOR.'database'.DIRECTORY_SEPARATOR.'migrations';
        BaseHandler::makeFile($folderPath, $fileName, $fileExtension = '.php', $migrationTemplate);
    }


    public static function getMigrationContain(){
        $rowArg = [];
        $previousKey = null;
        try {
            $migrationColumn = Builder::getMigrationColumns();
            foreach ($migrationColumn as $keys => $column){

                $columnArgs = explode('|',$column);
                if(in_array( 'default',$columnArgs) || in_array( 'comment',$columnArgs)){
                    foreach ($columnArgs as $key => $col){
                        if ($key == 0){
                            $row = '$table->'.$col."('".$keys."')";
                        }
                        if($key > 0 ){
                            if($col == 'default' || $col == 'comment' )
                            {
                                $row = $row.'->'.$col;
                                $previousKey = $key;
                            }
                            if(!is_null($previousKey) && $previousKey+1 == $key){
                                $row = $row.'('.$col.')';
                            }
                        }
                    }
                }
                else{
                    foreach ($columnArgs as $key => $col){
                        if ($key == 0){
                            $row = '$table->'.$col."('".$keys."')";
                        }
                        if($key > 0 ){
                            if($col != 'default' || $col != 'comment'){
                                $row = $row.'->'.$col.'()';
                            }
                        }
                    }
                }
                 array_push($rowArg,$row.';');
            }
            return implode(PHP_EOL, $rowArg );
        }
        catch (\Exception $ex){
            return 'Error in migration file process';
        }



    }

    public static function resolveFileName($tableName){
        $year = now()->year;
        $month = now()->format('m');
        $day = now()->format('d');
        $time = now()->format('his');
        return $year.'_'.$month.'_'.$day.'_'.$time.'_create_'.$tableName.'_table';
    }

    public static function resolveMigrationClassName($className){
        $name = Str::plural(strtolower($className));
        return str_replace('-','_',$name);
    }

    public static function solveStub($className, $migrationContain, $fileType){
        return str_replace(
            [
                '{MODEL_NAME}',
                '{FILLABLE_ATTR}',
                '{TABLE_NAME}',
            ],
            [
                Str::studly($className),
                $migrationContain,
                $className,
            ],
            BaseHandler::getStub($fileType)
        );
    }

}
