<?php


namespace App\Foundation\Supports\Generator\Handlers;


use App\Foundation\Supports\Generator\Handlers\View\Builder;
use Illuminate\Support\Str;

final class SeederFactoryHandler
{
    public static function getSeederFactory($argumentName){

        $className = BaseHandler::checkClassName($argumentName);
        BaseHandler::checkDuplicateEntry($modelName = $className, $fileName = 'factory');

        $fileName = Str::studly($className);

        $seederTemplate = self::solveStub($fileName, $fileType = 'Seeder');
        $attributes = self::columnHandler();

        $factoryTemplate = self::solveStubContain($fileName, $fileType = 'Factory', $attributes);

        $rootPath = base_path().DIRECTORY_SEPARATOR.'database'.DIRECTORY_SEPARATOR;
        $seederPath = $rootPath.'seeds';
        $factoryPath = $rootPath.'factories';

        BaseHandler::makeFile($seederPath, $fileName, $fileExtension = 'TableSeeder.php', $seederTemplate);
        BaseHandler::makeFile($factoryPath, $fileName, $fileExtension = 'Factory.php', $factoryTemplate);
    }


    public static function columnHandler(){
         $columnData = BaseHandler::reArrangeColumn(Builder::getFactory());
         foreach ($columnData  as $key => $column){
             $commonData[] = "'".$key."'".' => '.$column;
         }
        return implode ( ",".PHP_EOL.'    ', $commonData ) ;
    }

    public static function solveStub($capitalVariableName, $fileType){
            return str_replace(
            [
                '{modelName}',
            ],
            [
                $capitalVariableName,
            ],
                BaseHandler::getStub($fileType)
            );
    }

    public static function solveStubContain($capitalVariableName, $fileType, $modelAttributes){
        return str_replace(
            [
                '{modelName}',
                '{FILLABLE_ATTR}',
            ],
            [
                $capitalVariableName,
                $modelAttributes,
            ],
            BaseHandler::getStub($fileType)
        );
    }
}
