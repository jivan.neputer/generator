<?php


namespace App\Foundation\Supports\Generator\Handlers;


use Illuminate\Support\Str;

final class ModelHandler
{
    public static function getModel($argumentName){

        $className = BaseHandler::checkClassName($argumentName);
        BaseHandler::checkDuplicateEntry($modelName = $className, $fileName = 'model');
        $fileName = Str::studly($className);
        $modelAttributes = ColumnHandler::handle();
        $modelTableType = self::checkModelType($className);


        if($modelTableType == true):
            $protectedModel = self::resolveMultiClassName($className);
            $modelTemplate = self::solveStubProtectedModelContain($fileName, $fileType = 'ModelProtected', $modelAttributes, $protectedModel);
        else:
            $modelTemplate = self::solveStubContain($fileName, $filetype = 'Model', $modelAttributes);
        endif;

        $rootPath = app_path().DIRECTORY_SEPARATOR;
        BaseHandler::makeDirectory($rootPath , $fileType = 'Models');
        $folderPath = $rootPath.DIRECTORY_SEPARATOR.'Models';

        if($modelTableType == true):
            BaseHandler::makeFile($folderPath, $fileName, $fileExtension = '.php', $modelTemplate);
        else:
            BaseHandler::makeFile($folderPath, $fileName, $fileExtension = '.php', $modelTemplate);
        endif;

    }

    public static function resolveMultiClassName($className){
        $name = Str::plural(strtolower($className));
        return str_replace('-','_',$name);
    }
    public static function checkModelType($className){
        if(strpos($className, '-') !== false):
            return true;
        else:
            return false;
        endif;
    }

   public static function solveStubProtectedModelContain($capitalVariableName, $fileType, $modelAttributes,$protectedModel){
       return str_replace(
           [
               '{modelName}',
               '{FILLABLE_ATTR}',
               '{protectedModel}',
           ],
           [
               $capitalVariableName,
               $modelAttributes,
               $protectedModel,
           ],
           BaseHandler::getStub($fileType)
       );
   }

   public static function solveStubContain($capitalVariableName, $fileType, $modelAttributes){
       return str_replace(
           [
               '{modelName}',
               '{FILLABLE_ATTR}',
           ],
           [
               $capitalVariableName,
               $modelAttributes,
           ],
           BaseHandler::getStub($fileType)
       );
   }
}
