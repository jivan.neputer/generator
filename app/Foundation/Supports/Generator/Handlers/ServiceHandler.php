<?php


namespace App\Foundation\Supports\Generator\Handlers;


use Illuminate\Support\Str;

final class ServiceHandler
{
    public static function getService($argumentName){

        $className = BaseHandler::checkClassName($argumentName);
        BaseHandler::checkDuplicateEntry($modelName = $className, $fileName = 'service');
        $fileName = Str::studly($className);
        $variableName = lcfirst($fileName);
        $serviceTemplate = self::solveServiceStub($fileName, $fileType = 'Service', $variableName);

        $rootPath = app_path().DIRECTORY_SEPARATOR;
        BaseHandler::makeDirectory($rootPath , $fileType = 'Services');
        $folderPath = $rootPath.DIRECTORY_SEPARATOR.'Services';
        BaseHandler::makeFile($folderPath, $fileName, $fileExtension = 'Service.php', $serviceTemplate);
    }

    public static function solveServiceStub($modelName, $fileName, $variableName){
        return str_replace([
            '{MODEL_NAME}',
            '{VAR_CLASS_NAME}'
        ],
            [
                $modelName,
                $variableName,
            ],
            BaseHandler::getStub($fileName)
        );
    }

}
