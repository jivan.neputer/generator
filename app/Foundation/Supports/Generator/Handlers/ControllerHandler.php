<?php


namespace App\Foundation\Supports\Generator\Handlers;


use Illuminate\Support\Str;

final class ControllerHandler
{
    public static function getController($argumentName){

        $className = BaseHandler::checkClassName($argumentName);
        BaseHandler::checkDuplicateEntry($modelName = $className, $fileName = 'controller');
        $fileName = Str::studly($className);
        $controllerTemplate = self::solveStub($className, $fileType = 'Controller');
        $rootPath = app_path().DIRECTORY_SEPARATOR.'Http'.DIRECTORY_SEPARATOR.'Controllers'.DIRECTORY_SEPARATOR;
        BaseHandler::makeDirectory($rootPath , $folderName = 'Admin');
        $folderPath = $rootPath.DIRECTORY_SEPARATOR.'Admin';
        BaseHandler::makeFile($folderPath, $fileName, $fileType = 'Controller.php', $controllerTemplate);

    }

    public static function solveStub($className, $fileType){
        return str_replace(
            [
                '{MODEL_NAME}',
                '{VARIABLE_NAME}',
                '{ROUTE_NAME}',
            ],
            [
                Str::studly($className),
                lcfirst(Str::studly($className)),
                $className,
            ],
            BaseHandler::getStub($fileType)
        );
    }

}
