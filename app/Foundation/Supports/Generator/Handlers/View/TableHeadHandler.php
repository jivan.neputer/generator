<?php
namespace App\Foundation\Supports\Generator\Handlers\View;

use App\Foundation\Supports\Generator\Handlers\BaseHandler;

/**
 * Class TableHeadHandler
 * @package App\Foundation\Supports\Generator\Handlers
 */
class TableHeadHandler
{
    public static function handle()
    {
        $heads = array_map(function ($column) {
            return ucwords(str_replace("_", " ", $column));
        }, BaseHandler::reArrangeColumn(Builder::getView($type = 'table')));

        return "<th>" . implode ( "</th><th>", $heads ) . "</th>";
    }

}
