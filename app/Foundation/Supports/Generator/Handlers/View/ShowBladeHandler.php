<?php


namespace App\Foundation\Supports\Generator\Handlers\View;

use App\Foundation\Supports\Generator\Handlers\BaseHandler;

/**
 * Class ShowBladeHandler
 * @package App\Foundation\Supports\Generator\Handlers
 */
class ShowBladeHandler
{

    public static function handle(string $model)
    {
        $rows = array_map(function ($column) use ($model) {
            $COLUMN = ucwords(str_replace(['-', '_'], ' ', $column));
            return str_replace([ '{MODEL}', '{model}', '{COLUMN}', '{column}' ], [ ucwords($model) , strtolower($model), $COLUMN, strtolower($column) ],
                app('files')->get(app_path('Foundation/Views/row.blade.php')));
    }, BaseHandler::reArrangeColumn(Builder::getView($type = 'show')));
    return implode ( "", $rows );
    }
}
