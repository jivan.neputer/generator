<?php


namespace App\Foundation\Supports\Generator\Handlers\View;


use App\Foundation\Supports\Generator\Handlers\BaseHandler;
use Illuminate\Support\Str;

final class ViewHandler
{
    public static function getView($argumentName){

        $viewsCollection = ['create', 'edit', 'form', 'index', 'script', 'show', 'table'];
        $className = BaseHandler::checkClassName($argumentName);
//        BaseHandler::checkDuplicateEntry($modelName = $className, $fileName = 'view');

        $rootPath = base_path().DIRECTORY_SEPARATOR.'resources'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR;
        BaseHandler::makeDirectory($rootPath , $folderName = 'admin');
        $adminPath = $rootPath.DIRECTORY_SEPARATOR.'admin';

        BaseHandler::makeDirectory($adminPath , $folderName = $className);
        $folderPath = $adminPath.DIRECTORY_SEPARATOR.$className;
        $extension = '.blade.php';

        foreach ($viewsCollection as $key => $views) :
            $templateName = self::solveViewStub($className, $fileName = $views);

            if ($views == 'form' || $views == 'script' || $views == 'table'):
                BaseHandler::makeDirectory($folderPath , $fileName = 'partials');
                $partialPath = $folderPath.DIRECTORY_SEPARATOR.'partials';
                BaseHandler::makeFile($partialPath, $views, $fileType = $extension, $templateName);
            else:
                BaseHandler::makeFile($folderPath, $views, $fileType = $extension, $templateName);
            endif;
        endforeach;
    }

    public static function solveViewStub($className, $fileName)
    {
        if($fileName == 'form'):
            $dynamicForm = FormHandler::handle();
            return str_replace(
                [
                    '{DYNAMIC_FORM}',
                ],
                [
                    $dynamicForm,
                ],
                BaseHandler::getViewStub($fileName)
            );
        elseif($fileName == 'index'):
            $dataTableColumnHandler = DataTableColumnHandler::handle();
            return str_replace(
                [
                    '{modelName}',
                    '{LOWER_CLASS_NAME}',
                    '{CLASS_NAME}',
                    '{DATATABLE_COLUMNS}'
                ],
                [
                    $className,
                    strtolower($className),
                    Str::studly($className),
                    $dataTableColumnHandler
                ],
                BaseHandler::getViewStub($fileName)
            );
        elseif($fileName == 'table'):
            $tableHeader = TableHeadHandler::handle();
            return str_replace(
                [
                    '{TABLE_HEADS}',
                ],
                [
                    $tableHeader,
                ],
                BaseHandler::getViewStub($fileName)
            );
        elseif($fileName == 'show'):
            $showRows = ShowBladeHandler::handle($className);
            return str_replace(
                [
                    '{SHOW_ROWS}',
                    '{LOWER_CLASS_NAME}',
                    '{modelName}'
                ],
                [
                    $showRows,
                    strtolower($className),
                    $className
                ],
                BaseHandler::getViewStub($fileName)
            );
        else:
            return str_replace(
                [
                    '{modelName}',
                    '{LOWER_CLASS_NAME}'
                ],
                [
                    $className,
                    strtolower($className),
                ],
                BaseHandler::getViewStub($fileName)
            );
        endif;
    }

}
