<?php
namespace App\Foundation\Supports\Generator\Handlers\View;

use Illuminate\Support\Arr;
use Symfony\Component\Yaml\Yaml;

/**
 * Class Builder
 * @package App\Foundation\Supports\Generator\Handlers
 */
final class Builder
{
    public static function getForm(){
        return Yaml::parse(file_get_contents(base_path('form.yml')));
    }

    public static function getView($type){
         return self::getRequestViewData(self::getForm(),$type);

    }

    public static function getColumns(){
        $args = [];
        $commonData = [];
        $ymlData = self::getForm();
        foreach ($ymlData as $key => $data){
            if($key == 'column'){
                foreach ($data as $keyData => $model){
                    if($keyData == 'model'){
                        $commonData = $model;
                    }
                }
            }
        }
        return Arr::flatten(array_map(function ($item) use ($args) {
            return array_merge($args, explode('|', $item));
        },  array_values($commonData)));
    }

    public static function getFactory(){
        $commonData = [];
        $ymlData = self::getForm();
        foreach ($ymlData as $key => $data){
            if($key == 'column'){
                foreach ($data as $keyData => $factory){
                    if($keyData == 'factory'){
                        $commonData = $factory;
                    }
                }
            }
        }
        return $commonData;
    }

    public static function getMigrationColumns(){
        $commonData = [];
        $ymlData = self::getForm();
        foreach ($ymlData as $key => $data){
            if($key == 'column'){
                foreach ($data as $keyData => $migration){
                    if($keyData == 'migration'){
                        $commonData = $migration;
                    }
                }
            }
        }
        return $commonData;
    }

    public static function checkModelPermission(){
        $generate = [];
        $ymlData = self::getForm();
        foreach ($ymlData as $key => $data){
            if($key == 'generate'){
                $generate = $data;
            }
        }
        return $generate;
    }

    public static function getRequestViewData($ymlData, $type){
        foreach ($ymlData as $key => $data){
            if($key == 'view'){
                $commonData = $data;
                foreach ($commonData as $dataKey => $finalData){
                    if($type == 'form'){
                        if($dataKey == 'form'){
                            return  $finalData;
                        }
                    }
                    if($type == 'table' || $type == 'show'){
                        if($dataKey == 'showTable'){
                            return  self::manageViewData($finalData, $type);
                        }
                    }
                }
            }
        }
    }

    public static function manageViewData($finalData, $type){
        foreach ($finalData as $key => $data) {
            $checkData = explode('|', $data);
            if ($type == 'show'){
                if (in_array('show', $checkData)) {
                    $requestData[] = $key;
                }
            }
            if($type == 'table'){
                if(in_array('table', $checkData)){
                    $requestData[] = $key;
                }
            }

        }
        return $requestData;
    }

}
