<?php

namespace App\Foundation\Supports\Generator\Handlers\View;

use App\Foundation\Supports\Generator\Handlers\PathHandler;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

/**
 * Class FormHandler
 * @package App\Foundation\Supports\Generator\Handlers
 */
final class FormHandler
{
    /**
     * full location is " app('files')->get(realpath(base_path('app/Foundation/') " + "Views/Form/text"
     *
     * @var string
     */
    private static $_text = 'Views/Form/text';
    private static $_textarea = 'Views/Form/textarea';
    private static $_radio = 'Views/Form/Radio/virtualRadio';
    private static $_email = 'Views/Form/email';
    private static $_select = 'Views/Form/select';
    private static $_password = 'Views/Form/password';
    private static $_number = 'Views/Form/number';
    private static $_file = 'Views/Form/file';
    private static $_date = 'Views/Form/date';
    private static $_checkbox = 'Views/Form/Checkbox/virtualCheckbox';

    public static function handle()
    {
        $args = [];
        $selectedColumnKeys = [];
        foreach (Builder::getView($type = 'form') as $input => $columns) :
            $inputName = [];
            $columnName = [];
            $otherInputData = [];
            $placeholder = null;
            $prop = '_'.strtolower($input);
            $viewPath = get_class_vars(self::class)['_'.strtolower($input)];

            if (array_key_exists($prop, get_class_vars(self::class))) {
                    foreach (explode('|', $columns) as $colKey => $column) {
                        if ($input == 'radio' || $input == 'checkbox' ) {
                            $selectedColumnKeys[] = $input;
                           self::solveColumn($column, $colKey, $input);

                            $posFirst = self::stringPosition($column, '->');
                            $posSecond = self::stringPosition($column, '=>');

                            if ($posFirst < $posSecond) {
                                $expData = explode('->', $column);
                                $inputName[] = $expData[0];
                                if (Str::contains($expData[1], '=>')) {
                                    $expDataCol = explode('=>', $expData[1]);
                                    $columnName[] = $expDataCol[0];
                                } else {
                                    $columnName[] = $expData[1];
                                }
                            } else {
                                $expData = explode('=>', $column);
                                $otherInputData[] = $expData[0];
                            }
                        }
                        if ($input != 'radio' && $input != 'checkbox') {
                            $posFirst = self::stringPosition($column, '->');
                            $posSecond = self::stringPosition($column, '=>');
                            if ($posFirst != false && $posSecond != false && $posFirst < $posSecond) {
                                $expData = explode('=>', $column);
                                $firstData = $expData[0];
                                $secondData = $expData[1];

                                $expFirstData = explode('->', $firstData);
                                $inputName[] = $expFirstData[0];
                                $columnName[] = $expFirstData[1];

                                $expSecondData = explode('->', $secondData);
                                $placeholder = $expSecondData[1];
                            }
                            if ($posFirst != false && $posSecond != false && $posSecond < $posFirst ) {
                                $expData = explode('=>', $column);
                                $otherInputData[] = $expData[0];
                                $expSecondData = explode('->', $expData[1]);
                                $placeholder = $expSecondData[1];
                            }
                            if($posFirst != false && $posSecond == false){
                                $expData = explode('->', $column);
                                $inputName[] = $expData[0];
                                $columnName[] = $expData[1];
                            }
                            if($posFirst == false && $posSecond == false){
                                $expData = explode(',', $column);
                                $otherInputData[] = $expData[0];
                            }
                        }
                    }

                $formLabels = array_merge($columnName,$otherInputData);
                $formInputs = array_merge($inputName,$otherInputData);

                foreach ($formLabels as $key => $formLabel){
                    $formInput = $formInputs[$key];
                    $finalLabel = ucwords(str_replace(['-', '_'], ' ', $formLabel));

                    if($input == 'radio' || $input == 'checkbox' ){
                        $content = app('files')->get(realpath(base_path('app/Foundation/').$viewPath.$key.'.blade.php'));
                    }
                    if($input != 'radio' && $input != 'checkbox' ){
                        if(is_null($placeholder)){$placeholderValue =  null;}
                        else{
                            $placeholderValue = "'placeholder'=>'$placeholder'";}

                        $content = app('files')->get(realpath(base_path('app/Foundation/').$viewPath.'.blade.php'));
                    }
                    $args[] = str_replace([ 'labelName', 'columnName', 'placeholder'], [ $finalLabel, $formInput, $placeholderValue], $content);
                }

            }
        endforeach;;
        self::unlinkFile($selectedColumnKeys);
        return implode(PHP_EOL, $args);
    }


    /**
     * @param $column
     * @param $colKey
     * @param $input
     */
    public static function solveColumn($column, $colKey, $input){
        if( $input == 'radio') {$fileName = 'radioContent';} else{$fileName = 'checkboxContent';}
        $content = app('files')->get( self::getDirectory($folderName = $input , $fileName ));

        $expDatas = explode('=>', $column);
        foreach ($expDatas as $expKey => $singleData) {
            if ($expKey > 0) {
                $expSingleData = explode('->', $singleData);
                $capExpSingleData = ucwords($expSingleData[0]);
                if(is_numeric($expSingleData[1])){
                    if($expSingleData[1] == 1){
                        $trueFalse = "true";
                    }
                    else{
                        $trueFalse = "false";
                    }
                    $statusNum = $expSingleData[1];
                }
                if(!is_numeric($expSingleData[1])){
                    if($expKey == 1){
                        $trueFalse = "true";
                    }
                    else{
                        $trueFalse = "false";
                    }
                    $statusNum = "'$expSingleData[1]'";
                }

                $totalContent[] = str_replace(
                    [ 'type', 'label', 'statusNum', 'operation', 'idType'],
                    [ $expSingleData[0], $capExpSingleData, $statusNum, $trueFalse, $expSingleData[0]],
                    $content);

            }
        }

        self::createFile( $totalContent, $colKey, $input);

    }


    /**
     * @param $totalContent
     * @param $colKey
     * @param $input
     */
    public static function createFile($totalContent, $colKey, $input){
        if( $input == 'radio') {$fileName = 'radioTemplate';$virtualFileName = 'virtualRadio';}
        else{$fileName = 'checkboxTemplate';$virtualFileName = 'virtualCheckbox';}
        $fileContent = file_get_contents( self::getDirectory($folderName = $input, $fileName ));
        $radioData = str_replace(
            [
                '{AllContent}'
            ],
            [
                implode('', $totalContent)
            ],
            $fileContent
        );

        file_put_contents(
            self::getDirectory($folderName = $input, $fileName = $virtualFileName.$colKey),
            $radioData
        );
    }

    /**
     * @param $string
     * @param $symbol
     * @return false|int
     */
    public static function stringPosition($string, $symbol ){
        return strpos($string, $symbol);
    }

    /**
     * @param $folderName
     * @param $fileName
     * @return string
     */
    public static function getDirectory($folderName, $fileName){
        return PathHandler::getPath($folderName) . DIRECTORY_SEPARATOR . $fileName . '.blade.php';
    }

    /**
     * @param $input
     */
    public static function unlinkFile($input){
     foreach ($input as $file){
         if($file == 'radio'){$fileName = 'virtualRadio*.*';}else{$fileName = 'virtualCheckbox*.*';}
         $path = PathHandler::getPath($file). DIRECTORY_SEPARATOR;
         foreach (glob($path .$fileName) as $files) {
             unlink($files);
         }
     }
    }

}


