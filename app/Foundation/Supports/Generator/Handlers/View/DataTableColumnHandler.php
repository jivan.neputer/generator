<?php


namespace App\Foundation\Supports\Generator\Handlers\View;

use App\Foundation\Supports\Generator\Handlers\BaseHandler;

/**
 * Class DataTableColumnHandler
 * @package App\Foundation\Supports\Generator\Handlers
 */
final class DataTableColumnHandler
{

      public static function handle(){

          $heads = array_map(function ($column) {
              return strtolower( $column );
          }, BaseHandler::reArrangeColumn(Builder::getView($type = 'table')));

          return "{data: '" . implode ( "',orderable: true},{data: '", $heads ) . "',orderable: true},";

      }

}
