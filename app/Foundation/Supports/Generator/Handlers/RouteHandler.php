<?php

namespace App\Foundation\Supports\Generator\Handlers;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

/**
 * Class RouteHandler
 * @package Neputer\Supports\Generator\Handlers
 */
final class RouteHandler
{

    public static function getRoute($argumentName)
    {
        $className = BaseHandler::checkClassName($argumentName);
        BaseHandler::checkDuplicateEntry($modelName = $className, $fileName = 'route');
        $capName = Str::studly($argumentName);
        $lowName = strtolower($argumentName);
        $routeName = ('Route::resource(\'' . $lowName . "', '{$capName}Controller');\n");
        File::append(base_path('routes/admin.php'), $routeName);
    }

}
