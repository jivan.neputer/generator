<?php


namespace App\Foundation\Supports\Generator\Handlers;





use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

/**
 * Class BaseHandler
 * @package App\Foundation\Supports\Generator\Handlers
 */
final class BaseHandler
{
    /**
     * @param $type
     * @return false|string
     */
    public static function getStub($type){
        return file_get_contents(app_path("/Foundation/Supports/Generator/Console/stubs/$type.stub"));
    }


    /**
     * @param $type
     * @return false|string
     */
    public static function getViewStub($type){
        return file_get_contents(app_path("/Foundation/Supports/Generator/Console/stubs/views/$type.blade.stub"));
    }

    /**
     * @param $argumentName
     * @return mixed
     */
    public static function checkClassName($argumentName){
        if(preg_match('/[\'^£$%&*()}{@#~?><>,|=+¬_]/', $argumentName)){
            dd('Your model name should be like this : category or menu-category');
        }
        if(strtolower($argumentName) !== $argumentName){
            dd('Your model name should be in lower case like this : category or menu-category');
        }
        return $argumentName;
    }

    public static function checkDuplicateEntry($modelName,$fileName){
        $path = PathHandler::getPath($fileName);
        $resolveModelName = Str::studly($modelName);
        $snakeViewName = Str::snake($modelName);

        if($fileName == 'migration'){
            $migrationFiles = self::getFile($path);
            foreach ($migrationFiles as $migration){
                $tableName = explode('_create_', $migration);
                if($tableName[1] == $modelName.'s_table'){
                    dd($modelName.' migration is already exist!');
                }
            }
        }

        if($fileName == 'model'){
            $modelFiles = self::getFile($path);
            if(in_array($resolveModelName, $modelFiles)){
                dd($modelName.' model is already exist!');
            }
        }

        if($fileName == 'service'){
            $serviceFiles = self::getFile($path);
            $resolveServiceName = $resolveModelName.'Service';
            if(in_array($resolveServiceName, $serviceFiles)){
                dd($modelName.' service is already exist!');
            }
        }

        if($fileName == 'request'){
            $allDirectory = self::getDirectory($path);
            foreach ($allDirectory as $key => $directory){
                $requestFolderName = explode(PathHandler::getDirectory('request').'/',$directory);
                if($requestFolderName[1] == $resolveModelName){
                    dd($modelName.' request is already exist!');
                }
            }
        }

        if($fileName == 'factory'){
            $factoryFiles = self::getFile($path);
            $resolveFactoryName = $resolveModelName.'Factory';
            if(in_array($resolveFactoryName, $factoryFiles)){
                dd($modelName.' factory and seeder are already exist!');
            }
        }

        if($fileName == 'controller'){
            $controllerFiles = self::getFile($path);
            $resolveControllerName = $resolveModelName.'Controller';
            if(in_array($resolveControllerName, $controllerFiles)){
                dd($modelName.' controller is already exist!');
            }
        }

        if($fileName == 'view'){
            $allDirectory = self::getDirectory($path);
            foreach ($allDirectory as $key => $directory){
                $requestFolderName = explode('admin/',$directory);
                if($requestFolderName[1] == $snakeViewName){
                    dd($modelName.' view is already exist!');
                }
            }
        }

        if($fileName == 'appendMenu'){
            $content = self::getContent($path);
            $checkData = "(request()->is('admin/" . $snakeViewName ."*'))";
            if(strpos($content, $checkData) == true){
                dd($modelName.' menu is already append!');
            }
        }

        if($fileName == 'appendSeeder'){
            $content = self::getContent($path);
            $resolveControllerName = $resolveModelName.'TableSeeder';
            if(strpos($content, $resolveControllerName) == true){
                dd($modelName.' seeder is already append!');
            }

        }

        if($fileName == 'route'){
            $content = self::getContent($path);
            $resolveControllerName = $resolveModelName.'Controller';
            if(strpos($content, $resolveControllerName) == true){
                dd($modelName.' route is already exist!');
            }
        }
    }

    public static function getFile($path){
        $modelFiles = [];
        $allFiles = File::files($path);
        foreach ($allFiles as $key => $files){
            $file = pathinfo($files);
            $modelFiles[] = $file['filename'];
        }
        return $modelFiles;
    }

    public static function getDirectory($path){
        return File::directories($path);
    }

    public static function getContent($path){
        return File::get($path);
    }

    /**
     * @param $folderPath
     * @param $className
     * @param $fileType
     * @param $requestTemplate
     */
    public static function makeFile($folderPath, $className, $fileType, $requestTemplate){
        file_put_contents( $folderPath.DIRECTORY_SEPARATOR.$className.$fileType, $requestTemplate);
    }


    /**
     * @param $rootPath
     * @param $fileType
     * @param null $className
     */
    public static function makeDirectory($rootPath, $fileType, $className = null){
        if(!file_exists($path = $rootPath.DIRECTORY_SEPARATOR.$fileType)){
            mkdir($path, 0777, true);
        }
        if($className != null){
            if(!file_exists($folderPath = $path.DIRECTORY_SEPARATOR.$className)){
                mkdir($folderPath, 0777, true);
            }
        }
    }

    /**
     * @param $columnData
     * @return mixed
     */
    public static function reArrangeColumn($columnData){
        if(in_array( 'created_by',$columnData) || in_array( 'updated_by',$columnData) || in_array( 'slug',$columnData)){

            $result=array_search('slug',$columnData,true);
            if($result !== false) {
                $modifyData = $columnData[$result];
                unset($columnData[$result]);
                array_splice($columnData, 1, 0, $modifyData);
            }

            $result=array_search('created_by',$columnData,true);
            if($result !== false) {
                $modifyData = $columnData[$result];
                unset($columnData[$result]);
                array_push($columnData, $modifyData);
            }

            $result=array_search('updated_by',$columnData,true);
            if($result !== false) {
                $modifyData = $columnData[$result];
                unset($columnData[$result]);
                array_push($columnData, $modifyData);
            }
        }
       return $columnData;

    }

}
