<?php

namespace App\Console\Commands;

use App\Foundation\Supports\Generator\Handlers\BaseHandler;
use App\Foundation\Supports\Generator\Handlers\ControllerHandler;
use App\Foundation\Supports\Generator\Handlers\MigrationHandler;
use App\Foundation\Supports\Generator\Handlers\ModelHandler;
use App\Foundation\Supports\Generator\Handlers\RequestHandler;
use App\Foundation\Supports\Generator\Handlers\SeederFactoryHandler;
use App\Foundation\Supports\Generator\Handlers\seederHandler;
use App\Foundation\Supports\Generator\Handlers\ServiceHandler;
use App\Foundation\Supports\Generator\Handlers\RouteHandler;
use App\Foundation\Supports\Generator\Handlers\View\Builder;
use App\Foundation\Supports\Generator\Handlers\View\MenuHandler;
use App\Foundation\Supports\Generator\Handlers\View\ViewHandler;
use Illuminate\Console\Command;
use Illuminate\Support\Composer;

class Generator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * php artisan genji:generator car
     *
     * @var string
     */
    protected $signature = 'genji:generator {name : Class (singular)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate entities, migration, seeder, factories, routes,  model, requests, services, controller, views and appendMenu';

    /**
     * @var Composer
     */
    private $composer;


    /**
     * Generator constructor.
     * @param Composer $composer
     */
    public function __construct(Composer $composer)
    {
        parent::__construct();
        $this->composer = $composer;

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            $this->initGenerator();
            $this->composer->dumpAutoloads();
            $this->call('optimize:clear');
        }
        catch (\Exception $ex){
            $this->info($ex);
            $this->info('Generator Issue !!!');
        }

    }

    /**
     *
     */
    public function initGenerator(){
        self::checkExecuteModelPermission();
    }

    protected function checkExecuteModelPermission(){
        $checkModelData = Builder::checkModelPermission();
        foreach($checkModelData as $key => $generateData){
            if($key == 'migration' && $generateData == true){
                $this->migration();
            }
            if($key == 'model' && $generateData == true){
                $this->model();
            }
            if($key == 'request' && $generateData == true){
                $this->request();
            }
            if($key == 'seederAndFactories' && $generateData == true){
                $this->seederAndFactories();
            }
            if($key == 'routes' && $generateData == true){
                $this->routes();
            }
            if($key == 'service' && $generateData == true){
                $this->service();
            }
            if($key == 'controller' && $generateData == true){
                $this->controller();
            }
            if($key == 'view' && $generateData == true){
                $this->views();
            }
            if($key == 'appendMenu' && $generateData == true){
                $this->appendMenu();
            }
            if($key == 'appendSeeder' && $generateData == true){
                $this->appendSeeder();
            }

        }
    }

    protected function migration(){
        Builder::checkModelPermission();
        $argumentName = $this->argument('name');
        MigrationHandler::getMigration($argumentName);
        $this->info('Generator has successfully generated the Migration for you.');
    }

    /**
     *
     */
    protected function model()
    {
        $argumentName = $this->argument('name');
        ModelHandler::getModel($argumentName);
        $this->info('Generator has successfully generated the Model for you.');
    }

    /**
     *
     */
    protected function request()
    {
        $argumentName = $this->argument('name');
        RequestHandler::getRequest($argumentName);
        $this->info('Generator has successfully generated the Request you.');
    }


    /**
     *
     */
    protected function seederAndFactories()
    {
        $argumentName = $this->argument('name');
        SeederFactoryHandler::getSeederFactory($argumentName);
        $this->info('Generator has successfully generated the Seeder and Factory you.');
    }

    /**
     *
     */
    protected function routes(){
        $argumentName = $this->argument('name');
        RouteHandler::getRoute($argumentName);
        $this->info('Generator has successfully appended routes for you.');
    }

    /**
     *
     */
    protected function service(){
        $argumentName = $this->argument('name');
        ServiceHandler::getService($argumentName);
        $this->info('Generator has successfully generated the service for you.');
    }

    /**
     *
     */
    protected function controller()
    {
        $argumentName = $this->argument('name');
        ControllerHandler::getController($argumentName);
        $this->info('Generator has successfully generated the Controller for you.');
    }

    protected function appendMenu(){
        $argumentName = $this->argument('name');
        MenuHandler::handle($argumentName);
        $this->info('Generator has successfully appended menu for you.');
    }

    protected function views(){
        $argumentName = $this->argument('name');
        ViewHandler::getView($argumentName);
        $this->info('Generator has successfully generated the view for you.');
    }

    protected function appendSeeder(){
        $argumentName = $this->argument('name');
        seederHandler::handle($argumentName);
    }


}
