<?php

namespace App\Generator\Base;



use App\Generator\Concerns\CrudTrait;

/**
 * Class BaseService
 * @package Neputer\Supports
 */
abstract class BaseService
{

    use CrudTrait;

}
